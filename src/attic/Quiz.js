import React, { Component } from 'react';
import Question from './SimpleQuestion.js';
import random from './utils.js';

class Quiz extends Component {
  constructor(props) {
    super(props);
    this.state = {exercises: [], mark: false};

    this.mark = this.mark.bind(this);
  }

  componentDidMount() {
    let exercises = [];

    for (let i = 0; i < 10; ++i) {
      // let sum = random(1, 10);
      // let a = random(1, sum);
  
      // exercises.push({expression: `${a} + ${sum - a}`, answer: sum});

      let sum = random(1, 10);
      let a = random(1, sum);
  
      exercises.push({expression: `${sum} - ${a}`, answer: sum - a});
    }

    this.setState({exercises: exercises});
  }

  mark(event) {
    this.setState({mark: true});
    event.preventDefault();
  }

  render() {
    const questions = this.state.exercises.map((e, i) => <Question key={i} expression={e.expression} answer={e.answer} mark={this.state.mark}/>);

    return (
      <div className="Quiz">
        <form>
          {questions}
          <p/>
          <button onClick={this.mark}>Rätta</button>
        </form>
      </div>
    );
  }
}

export default Quiz;
