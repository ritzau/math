import React from 'react';

import { Input } from 'semantic-ui-react'

const Question = ({ id, expression, answer, savedAnswer, onChange }) => {
  return (
    <div className="Question">
      <div>{expression + ' = '}</div>
      <div><Input type='number' size='small' placeholder='Svar' onChange={e => onChange(id, parseInt(e.target.value, 10))} /></div>
      <div>{Number.isNaN(savedAnswer) ? '' : savedAnswer === answer ? '🦄' : '💩'}</div>
    </div>
  );
}

export default Question;
