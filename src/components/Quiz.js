import React from 'react';
import PropTypes from 'prop-types'

import { Button, Container, Form, Header } from 'semantic-ui-react'

import FooQuestion from '../containers/FooQuestion.js';
import TabsContainer from '../containers/TabsCointainer.js';

const Quiz = ({level, quizzes, onMarkClick, onResetClick}) => {
  const questions = quizzes.map((e, i) => <FooQuestion key={e.id} id={e.id} />);

  return (
    <Container>
      <Header as='h1'>Viggos matte</Header>
      <TabsContainer />
      <Form>
        <div className='questions'>
          {questions}
        </div>
        <p/>
        <Button primary onClick={onMarkClick}>Rätta</Button>
        <Button secondary onClick={e => onResetClick(level)}>Återställ</Button>
      </Form>
    </Container>
  );
}

Quiz.propTypes = {
  quizzes: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      expression: PropTypes.string.isRequired,
      answer: PropTypes.number.isRequired,
      savedAnswer: PropTypes.number,
    }).isRequired
  ).isRequired,
  onMarkClick: PropTypes.func.isRequired
}

export default Quiz;
