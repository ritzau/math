import React from 'react'
// import Footer from './Footer'
// import AddTodo from '../containers/AddTodo'
// import VisibleTodoList from '../containers/VisibleTodoList'
import PopulatedQuiz from '../containers/PopulatedQuiz'

const App = () => (
  <div>
    {/* <AddTodo />
    <VisibleTodoList />
    <Footer /> */}
    <PopulatedQuiz />
  </div>
)

export default App