import React from 'react'
import { Menu } from 'semantic-ui-react'

const Tabs = ({activeItem, onItemClick}) => (
    <Menu pointing secondary>
        <Menu.Item name='Level 1' active={activeItem === 0} onClick={e => onItemClick(0)} />
        <Menu.Item name='Level 2' active={activeItem === 1} onClick={e => onItemClick(1)} />
        <Menu.Item name='Level 3' active={activeItem === 2} onClick={e => onItemClick(2)} />
    </Menu>
);

export default Tabs;