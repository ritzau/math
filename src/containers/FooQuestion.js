import { connect } from 'react-redux'

import { updateAnswer } from '../actions'
import Question from '../components/Question'

const mapStateToProps = (state, ownProps) => {
  const quiz = state.quizzes.find(q => q.id === ownProps.id);

  return {
    id: ownProps.id,
    expression: quiz.expression,
    answer: quiz.answer,
    savedAnswer: quiz.savedAnswer,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onChange: (id, value) => {
      dispatch(updateAnswer(id, value))
    }
  }
}

const FooQuestion = connect(
  mapStateToProps,
  mapDispatchToProps
)(Question);

export default FooQuestion;
