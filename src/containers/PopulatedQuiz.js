import { connect } from 'react-redux'

import { markQuiz, setLevel } from '../actions'
import Quiz from '../components/Quiz'

const mapStateToProps = state => {
  return {
    level: state.level,
    quizzes: state.quizzes,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onMarkClick: () => {
      dispatch(markQuiz())
    },
    onResetClick: level => {
      dispatch(setLevel(level))
    },
  }
}

const PopulatedQuiz = connect(
  mapStateToProps,
  mapDispatchToProps
)(Quiz)

export default PopulatedQuiz