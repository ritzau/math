import { connect } from 'react-redux'

import { setLevel } from '../actions'
import Tabs from '../components/Tabs'

const mapStateToProps = state => {
  return {
    activeItem: state.level,
  };
}

const mapDispatchToProps = dispatch => {
  return {
      onItemClick: item => dispatch(setLevel(item)),
  };
}

const TabsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Tabs)

export default TabsContainer