import generateQuiz from '../quiz-generator'

let nextQuizId = 0;

function initialState(level = 0) {
  const levelTags = [
    'level:0',
    'level:1',
    'level:2',
  ];

  return generateQuiz(['math', levelTags[level]], 10).map(q => ({
      id: nextQuizId++,
      expression: q.question,
      answer: q.answer,
      value: Number.NaN,
      savedAnswer: Number.NaN,
  }));
}

const quizzes = (state = initialState(0), action) => {
    switch (action.type) {
      case 'ADD_QUIZ':
        return [
          ...state,
          {
            id: action.id,
            expression: action.expression,
            answer: action.answer,
            value: Number.NaN,
            savedAnswer: Number.NaN,
          }
        ];

      case 'MARK_QUIZ':
        return state.map(quiz => ({...quiz, savedAnswer: quiz.value}));

      case 'UPDATE_ANSWER':
        return state.map(quiz =>
            (quiz.id === action.id) 
                ? {...quiz, savedAnswer: Number.NaN, value: action.value}
                : quiz
        );

        case 'SET_LEVEL':
        return initialState(action.level);

        case 'RESET_QUIZ':
          return initialState(state.level);

      default:
        return state
    }
}
  
export default quizzes
  