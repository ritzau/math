import { combineReducers } from 'redux'

import quizzes from './quizzes'
import level from './level'

const todoApp = combineReducers({
  level,
  quizzes,
})

export default todoApp