import { random } from '../utils'

const quizDatabase = new Map();

function importQuiz(tags, generator) {
    tags.forEach(tag => {
        let set = quizDatabase.get(tag);
        if (set === undefined) {
            set = new Set();
            quizDatabase.set(tag, set);
        }
        set.add(generator);
    });
}

function getQuiz(tags) {
    const set = new Set();

    if (tags.length === 0) return set;

    const generators = tags.map(t => quizDatabase.get(t));
    const firstSet = generators.shift()

    return generators.reduce((a, v) => [...a].filter(f => v.has(f)), firstSet);
}

function populateDatabase() {
    importQuiz(['math', 'arithmetic', 'addition', 'level:0'], () => {
        let sum = random(1, 10);
        let a = random(1, sum);
      
        let question = `${a} + ${sum - a}`;
        let answer = sum;

        return {question, answer};
    });

    importQuiz(['math', 'arithmetic', 'subtraction', 'level:0'], () => {
        let sum = random(1, 10);
        let a = random(1, sum);
      
        let question = `${sum} - ${a}`;
        let answer = sum - a;

        return {question, answer};
    })

    importQuiz(['math', 'arithmetic', 'addition', 'level:1'], () => {
        let sum = random(11, 20);
        let a = random(5, sum - 5);
      
        let question = `${a} + ${sum - a}`;
        let answer = sum;

        return {question, answer};
    });

    importQuiz(['math', 'arithmetic', 'subtraction', 'level:1'], () => {
        let sum = random(11, 20);
        let a = random(sum - 10, sum - 5);
      
        let question = `${sum} - ${a}`;
        let answer = sum - a;

        return {question, answer};
    })

    importQuiz(['math', 'arithmetic', 'multiplication', 'level:1'], () => {
        let a = random(1, 4);
        let b = random(1, 4);
      
        let question = `${a} × ${b}`;
        let answer = a * b;

        return {question, answer};
    })

    importQuiz(['math', 'arithmetic', 'division', 'level:1'], () => {
        let a = random(1, 4);
        let b = random(1, 4);
      
        let question = `${a*b} ÷ ${a}`;
        let answer = b;

        return {question, answer};
    })

    importQuiz(['math', 'arithmetic', 'addition', 'level:2'], () => {
        let sum = random(201, 999);
        let a = random(101, sum - 101);
      
        let question = `${a} + ${sum - a}`;
        let answer = sum;

        return {question, answer};
    });

    importQuiz(['math', 'arithmetic', 'subtraction', 'level:2'], () => {
        let sum = random(201, 999);
        let a = random(101, sum - 101);
      
        let question = `${sum} - ${a}`;
        let answer = sum - a;

        return {question, answer};
    })

    importQuiz(['math', 'arithmetic', 'multiplication', 'level:2'], () => {
        let a = random(11, 20);
        let b = random(2, 9);
      
        let question = `${a} × ${b}`;
        let answer = a * b;

        return {question, answer};
    })

    importQuiz(['math', 'arithmetic', 'division', 'level:2'], () => {
        let a = random(11, 20);
        let b = random(2, 9);
      
        let question = `${a*b} ÷ ${a}`;
        let answer = b;

        return {question, answer};
    })
}

function generateQuiz(tags, count) {
    const questions = [];
    if (quizDatabase.size === 0) populateDatabase();

    const generators = getQuiz(tags);
    if (generators.size === 0) return questions;

    const generatorArray = new Array(...generators);
    for (let i = 0; i < count; ++i) {
        questions.push(generatorArray[random(0, generatorArray.length - 1)]()); 
    }

    return questions;
}

export default generateQuiz;