class Topic {
    constructor({id, title, tags, prerequisites}) {
        this._id = id
        this._title = title
        this._tags = tags
        this._prerequisites = prerequisites
    }

    get id() {
        return this._id
    }

    get title() {
        return this._title
    }

    get tags() {
        return this._tags
    }

    get prerequisites() {
        return this._prerequisites
    }

    // get theoryUrl() {
    //     return 
    // }

    // get quizUrl() {
    //     return
    // }
}

class Module {
    get id() {

    }

    get title() {

    }

    get tags() {
        return []
    }
    
    get topicIds() {
        return []
    }
}

class TopicDatabase {
    fetchModule(id) {
        return new Promise((resolve, reject) => {

        })
    }

    fetchTopic(id) {
        return new Promise((resolve, reject) => {

        })
    }

    fetchModulesByTags(tags) {
        return new Promise((resolve, reject) => {

        })
    }

    fetchTopicsByTags(tags) {
        return new Promise((resolve, reject) => {

        })
    }
}