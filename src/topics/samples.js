const sampleTopics = [
    {
        id: 0,
        title: 'Introduktion till dom fyro räknesätten',
        tags: ['math', 'K-3', 'arithmetic'],
        dependencies: [],
        theoryUrl: '/topic/0',
        quizUrl: '/quiz/0',
    },
    {
        id: 1,
        title: 'Addition med ensiffriga tal',
        dependencies: [0],
    },
]

const sampleModules = [
    {
        id: 0,
        title: 'De fyra räknesätten',
        tags: ['math', 'K-3', 'arithmetic'],
    },
]