let nextTodoId = 0;
// let nextQuizId = 0;

export const addTodo = text => {
  return {
    type: 'ADD_TODO',
    id: nextTodoId++,
    text
  }
}

export const setVisibilityFilter = filter => {
  return {
    type: 'SET_VISIBILITY_FILTER',
    filter
  }
}

export const toggleTodo = id => {
  return {
    type: 'TOGGLE_TODO',
    id
  }
}

// export const addQuiz = (expression, answer) => {
//   return {
//     type: 'ADD_QUIZ',
//     id: nextQuizId++,
//     expression,
//     answer,
//   }
// }

export const updateAnswer = (id, value) => {
  return {
    type: 'UPDATE_ANSWER',
    id,
    value,
  }
}

export const setLevel = (level) => {
  return {
    type: 'SET_LEVEL',
    level: level,
  }
}

export const resetQuiz = () => {
  return {
    type: 'RESET_QUIZ',
  }
}

export const markQuiz = () => {
  return {
    type: 'MARK_QUIZ',
  }
}