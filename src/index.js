import React from 'react';
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'

import 'semantic-ui-css/semantic.min.css';
import './index.css';

import todoApp from './reducers'
import App from './components/App'

let store = createStore(todoApp)
// store.subscribe(() => console.log(store.getState()))

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)

// registerServiceWorker();
